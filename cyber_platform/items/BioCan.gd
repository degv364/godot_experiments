extends Area2D

var consumed = false

# Called when the node enters the scene tree for the first time.
func _ready():
	$AnimatedSprite.play("default")
	pass # Replace with function body.


func _on_BioCan_body_entered(body):
	if body.name == "Player" and not consumed:
		body.receive_heal()
		$audio.play()
		$AnimatedSprite.play("heal")
		consumed = true

func _on_AnimatedSprite_animation_finished():
	if consumed == true:
		queue_free()
