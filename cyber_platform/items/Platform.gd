extends Node2D

export var vertical = false
export var speed_move = 50
export var cage_size: int = 192

export var forward = true
var velocity = Vector2.ZERO

# Called when the node enters the scene tree for the first time.
func _ready():
	$bounds.scale = Vector2(cage_size/192, cage_size/192)
	pass # Replace with function body.


func _physics_process(delta):
	velocity.x = 0
	velocity.y = 0
	
	if vertical:
		if forward:
			velocity.y = speed_move
		else:
			velocity.y = -speed_move
	else:
		if forward:
			velocity.x = speed_move
		else:
			velocity.x = -speed_move
	
	velocity = $moving_platform.move_and_slide(velocity, Vector2.UP)

func _on_bounds_body_exited(body):
	forward = not forward
