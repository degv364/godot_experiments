extends Area2D

var picked = false
# Called when the node enters the scene tree for the first time.
func _ready():
	$AnimatedSprite.play("idle")

func _on_Coin_body_entered(body):
	if body.name == "Player" and not picked:
		body.pick_coin()
		$AnimatedSprite.play("pick")
		$noise.play()
		picked = true


func _on_AnimatedSprite_animation_finished():
	if picked:
		queue_free()
