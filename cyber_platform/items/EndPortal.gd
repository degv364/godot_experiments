extends Area2D

signal player_in_portal
signal finish

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func turn_on():
	$LightOn.visible = true
	$energy.visible = true
	$LightOff.visible = false
	$on_sound.play()
	$Timer.start()
	

func _on_EndPortal_body_entered(body):
	if body.name == "Player":
		emit_signal("player_in_portal")


func _on_Timer_timeout():
	emit_signal("finish")
