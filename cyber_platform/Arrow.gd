extends RigidBody2D

# Called when the node enters the scene tree for the first time.
func _ready():
	pass
	
func flip_left():
	$Sprite.flip_h = true
	
func flip_right():
	$Sprite.flip_h = false


func _on_life_timeout():
	queue_free()
