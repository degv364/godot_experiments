extends CanvasLayer

signal quit_level

export var total_score = 1

var lives = 3
var score = 0

func _ready():
	$ScoreContainer/Label.text = "0/" + str(total_score)

func take_damage():
	if lives == 3:
		$GridContainer/Heart3.play("explode")
		lives = 2
	elif lives == 2:
		$GridContainer/Heart2.play("explode")
		lives = 1
	elif lives == 1:
		$GridContainer/Heart1.play("explode")
		lives = 0

func take_heal():
	if lives == 1:
		lives = 2
		$GridContainer/Heart2.play("idle")
	elif lives == 2:
		lives = 3
		$GridContainer/Heart3.play("idle")

func update_total_score(new_total):
	total_score = new_total
	$ScoreContainer/Label.text = str(score) + "/" + str(total_score)
	$ScoreContainer/Label.show()

func update_ammunitions(new_total):
	$Ammunitions/Label.text = str(new_total)
	$Ammunitions/Label.show()


func score_up():
	score += 1
	$ScoreContainer/Label.text = str(score) + "/" + str(total_score)
	$ScoreContainer/Label.show()


func _on_TextureButton_pressed():
	emit_signal("quit_level")
