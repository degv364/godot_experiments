extends Node

signal finish(success)

export var score_total = 3
export (PackedScene) var Arrow
export var ammunitions = 5
export var arrow_speed = 1000

var lives = 3

# Called when the node enters the scene tree for the first time.
func _ready():
	$CanvasModulate.visible = true
	$Ui.update_total_score(score_total)
	$Ui.update_ammunitions(ammunitions)

func take_damage():
	if lives > 0:
		lives -= 1
		$Ui.take_damage()

func _on_Player_shoot():
	if ammunitions != 0:
		ammunitions -= 1
		var arrow = Arrow.instance()
		arrow.position = $Player.position
		arrow.linear_velocity.y = 0
		$Ui.update_ammunitions(ammunitions)
		if $Player.right:
			arrow.position.x += 8
			arrow.linear_velocity.x = arrow_speed
			arrow.flip_right()
		else:
			arrow.position.x += 8
			arrow.linear_velocity.x = -arrow_speed
			arrow.flip_left()
		add_child(arrow)

func _on_Player_hurt():
	take_damage()

func _on_Player_heal():
	if lives < 3:
		lives += 1
		$Ui.take_heal()

func _on_Player_score_up():
	$Ui.score_up()


func _on_Player_death():
	emit_signal("finish", false)


func _on_EndPortal_player_in_portal():
	if $Ui.score == $Ui.total_score:
		$items/EndPortal.turn_on()

func _on_EndPortal_finish():
	emit_signal("finish", true)


func _on_DeepDeath_body_entered(body):
	if body.name == "Player":
		emit_signal("finish", false)


func _on_Ui_quit_level():
	emit_signal("finish", false)
