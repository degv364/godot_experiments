# Credits

Hi I'm Daniel. I made this game with Godot, and it is open source. You can find
the source code here: https://gitlab.com/degv364/godot_experiments . 

Most of the assets were made by other people. Although all assets are free and
Most are licensed under creative commons, there are some that restrict
distribution of source (they can only be distributed as part of a project). So
the entire assets directory is not uploaded to git.

## Assets
- [0x72](https://0x72.itch.io/)
- [AstroBob](https://astrobob.itch.io/arcane-archer)
- [Penusbmic](https://penusbmic.itch.io/)
- [Hugues Laborde](https://hugues-laborde.itch.io/)
- [Kenney](www.kenney.nl)
- [Ansimuz](https://ansimuz.itch.io/)
- [Rvros](https://rvros.itch.io/)
- [Pimen](https://pimen.itch.io/)
- [Code Manu](https://codemanu.itch.io/)
- [Stealthix](https://stealthix.itch.io/)

## Shaders
- [Gonkee](https://www.youtube.com/channel/UCJqCPFHdbc6443G3Sz6VYDw)
- [Yui Kinomoto](https://godotshaders.com/author/arlez80/)

## Audio
- [Timothy Adan](https://timothyadan.itch.io/)
- [Goose Ninja](https://gooseninja.itch.io/)
- [Luis Zuno](ansimuz.com)
- [void1](www.void1gaming.com)
