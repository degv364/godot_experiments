extends KinematicBody2D

export var move_speed = 50
export var gravity = 2000

var attacking = false
var player_danger = false
var already_dealt_damage = false
var death = false
var facing_right = true

var velocity = Vector2.ZERO

var player

# Called when the node enters the scene tree for the first time.
func _ready():
	$attack_time.wait_time = randi() % 3 + 2
	$AnimatedSprite.play("walk")
	$attack_time.start()
	
	
func can_continue_right() -> bool:
	return $floor_right.is_colliding() and not $wall_right.is_colliding()

func can_continue_left() -> bool:
	return $floor_left.is_colliding() and not $wall_left.is_colliding()
	
func _physics_process(delta: float) -> void:
	# reset horizontal velocity
	velocity.x = 0
	
	if attacking:
		velocity.x = 0
	else:
		if facing_right and can_continue_right():
			velocity.x = move_speed
		if facing_right and not can_continue_right():
			facing_right = false
			velocity.x = 0
		if not facing_right and can_continue_left():
			velocity.x = -move_speed
		if not facing_right and not can_continue_left():
			facing_right = true
			velocity.x = 0

	velocity.y += gravity * delta

	# actually move the player
	velocity = move_and_slide(velocity, Vector2.UP)

func _process(delta):
	if attacking and player_danger and not already_dealt_damage:
		player.receive_damage()
		already_dealt_damage = true

func _on_attack_time_timeout():
	$attack_time.wait_time = randi() % 3 + 2
	already_dealt_damage = false
	$attack_light.visible = true
	attacking = true
	$shock_sound.play()
	if not death:
		$AnimatedSprite.play("attack")


func _on_AnimatedSprite_animation_finished():
	if death:
		queue_free()
	else:
		attacking = false
		$AnimatedSprite.play("walk")
		$attack_light.visible = false


func _on_Area2D_body_entered(body):
	if body.name == "Player":
		player = body
		player_danger = true

func _on_Area2D_body_exited(body):
	if body.name == "Player":
		player_danger = false


func _on_health_area_body_entered(body):
	if body.name == "Arrow" or body.name == "Player":
		death = true
		$death_sound.play()
		$AnimatedSprite.play("die")
	if body.name == "Player":
		body.receive_damage()
