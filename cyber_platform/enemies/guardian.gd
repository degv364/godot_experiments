extends RigidBody2D


var attacking = false
var player_danger = false
var player

# Called when the node enters the scene tree for the first time.
func _ready():
	$attack_time.wait_time = randi() % 3 + 2
	$AnimatedSprite.play("idle")
	$attack_time.start()
	
func _process(delta):
	if attacking and player_danger:
		player.receive_damage()

func _on_AnimatedSprite_animation_finished():
	attacking = false
	$AnimatedSprite.play("idle")
	$attack_light.visible = false


func _on_attack_time_timeout():
	$attack_time.wait_time = randi() % 3 + 2
	$attack_light.visible = true
	attacking = true
	$AnimatedSprite.play("attack")
	$attack_sound.play()


func _on_guardian_body_entered(body):
	if body.name == "Player" or body.name == "Arrow":
		$attack_time.stop()
		$AnimatedSprite.visible = false
		$death_fire.visible = true
		$death_light.visible = true
		$death_sound.play()
		$death_fire.play("default")


func _on_death_fire_animation_finished():
	queue_free()


func _on_Area2D_body_entered(body):
	if body.name == "Player":
		player = body
		player_danger = true


func _on_Area2D_body_exited(body):
	if body.name == "Player":
		player_danger = true
