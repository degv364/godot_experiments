extends Node2D

export (PackedScene) var Plasma
export var color_type = "green"
export var plasma_speed = 300
export var min_period = 7
export var random_spread = 3

# Called when the node enters the scene tree for the first time.
func _ready():
	$shoot_sound.stream.loop = false
	$ShootTimer.wait_time = randi() % random_spread + min_period
	$ShootTimer.start()

func _on_ShootTimer_timeout():
	$ShootTimer.wait_time = randi() % random_spread + min_period
	$shoot_sound.play()
	var plasma = Plasma.instance()
	plasma.set_type(color_type)
	plasma.set_velocity(plasma_speed, rotation)
	add_child(plasma)
