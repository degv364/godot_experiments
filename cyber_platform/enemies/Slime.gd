extends KinematicBody2D

export var facing_right = false
export var gravity = 1500
export var velocity = Vector2.ZERO
export var move_speed = 50

var dying = false

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func can_continue_right() -> bool:
	return $floor_right.is_colliding() and not $wall_right.is_colliding()
	
func can_continue_left() -> bool:
	return $floor_left.is_colliding() and not $wall_left.is_colliding()

func die():
	queue_free()

func _physics_process(delta):
	velocity.x = 0
	velocity.y += gravity * delta
	
	if is_on_floor():
		$Figure.play("walk")
		if facing_right and can_continue_right():
			velocity.x = move_speed
		if facing_right and not can_continue_right():
			facing_right = false
			$Figure.flip_h = true
			velocity.x = -move_speed
		if not facing_right and can_continue_left():
			velocity.x = -move_speed
		if not facing_right and not can_continue_left():
			$Figure.flip_h = false
			velocity.x = move_speed
			facing_right = true

	elif is_on_ceiling():
		# If is on the ceiling remove movement
		$Figure.play("idle")
		$Figure.flip_v = true
		velocity.x = 0
		
	else:
		$Figure.flip_v = false
		$Figure.play("fall")

	# actually move the player
	velocity = move_and_slide(velocity, Vector2.UP)

func _on_DamageArea_body_entered(body):
	if is_on_floor() or is_on_ceiling():
		if body.name == "Player":
			die()
			body.receive_damage()


func _on_DetectionArea_body_entered(body):
	if body.name == "Player" and is_on_ceiling():
		gravity = -gravity


func _on_FallArea_body_entered(body):
	if not is_on_ceiling() and not is_on_floor():
		if body.name == "Player":
			die()
			body.receive_damage()


func _on_Figure_animation_finished():
	if dying:
		queue_free()
