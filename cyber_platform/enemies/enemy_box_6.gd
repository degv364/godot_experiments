extends KinematicBody2D

export var move_speed = 50
export var gravity = 2000

var collision_occured = false
var facing_right = true

var velocity = Vector2.ZERO

# Called when the node enters the scene tree for the first time.
func _ready():
	$body.play("walk")
	
func can_continue_right() -> bool:
	return $right_floor_ray.is_colliding() and not $right_wall_ray.is_colliding()
	
func can_continue_left() -> bool:
	return $left_floor_ray.is_colliding() and not $left_wall_ray.is_colliding()
	
func _physics_process(delta):
	velocity.x = 0
	velocity.y += gravity * delta
	
	if facing_right and can_continue_right():
		velocity.x = move_speed
	if facing_right and not can_continue_right():
		facing_right = false
		$body.flip_h = true
		velocity.x = -move_speed
	if not facing_right and can_continue_left():
		velocity.x = -move_speed
	if not facing_right and not can_continue_left():
		$body.flip_h = false
		velocity.x = move_speed
		facing_right = true

	# actually move the player
	velocity = move_and_slide(velocity, Vector2.UP)

func _on_body_animation_finished():
	if collision_occured:
		queue_free()


func _on_talk_finished():
	$silence_timer.start()

func _on_silence_timer_timeout():
	$silence_timer.wait_time = randi() % 3 + 2
	$talk.play()

func _on_RobotArea_body_entered(entruder):
	if entruder.name == "Arrow" or entruder.name == "Player":
		collision_occured = true
		$explode_light.visible = true
		$explode.play()
		$body.play("explode")


func _on_DamageArea_body_entered(entruder):
	if entruder.name == "Player":
		collision_occured = true
		$explode_light.visible = true
		$explode.play()
		$body.play("explode")
		entruder.receive_damage()
