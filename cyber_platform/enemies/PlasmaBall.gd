extends RigidBody2D

var color = "green"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func set_type(type):
	if type == "green":
		$GreenLight.visible = true
		$Plasma.play("green")
	else:
		$BlueLight.visible = true
		$Plasma.play("blue")

func set_velocity(speed, orientation):
	linear_velocity = Vector2(speed, 0).rotated(orientation)
		

func _on_PlasmaBall_body_entered(body):
	if body.name == "Player":
		body.receive_damage()
	if body.name == "Arrow":
		$ArrowHit.restart()
	else:
		queue_free()


func _on_Buzz_finished():
	#Destroy itself qhen there is no sound
	queue_free()
