extends Area2D

var damaging
var player


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.



func _process(delta):
	if damaging:
		player.receive_damage()

func _on_burner_body_entered(body):
	if body.name == "Player":
		damaging = true
		player = body


func _on_burner_body_exited(body):
	if body.name == "Player":
		damaging = false
		player = body
