extends KinematicBody2D

signal shoot
signal hurt
signal heal
signal score_up
signal death

export var move_speed = 150
export var gravity = 2000
export var jump_speed = -600
export var lives = 3

var right = true
var velocity = Vector2.ZERO
var glitching = false
var dying = false

func _physics_process(delta: float) -> void:
	# reset horizontal velocity
	velocity.x = 0

	# set horizontal velocity
	if Input.is_action_pressed("ui_right"):
		velocity.x += move_speed

	if Input.is_action_pressed("ui_left"):
		velocity.x -= move_speed
	
	if Input.is_action_pressed("ui_up"):
		if is_on_floor():
			velocity.y = jump_speed

	velocity.y += gravity * delta

	# actually move the player
	velocity = move_and_slide(velocity, Vector2.UP)
	
func _process(delta):
	if Input.is_action_pressed("ui_left"):
		$AnimatedSprite.set_flip_h(true)
		right = false
		if is_on_floor() and not glitching:
			$AnimatedSprite.play("run")
	if Input.is_action_pressed("ui_right"):
		$AnimatedSprite.set_flip_h(false)
		right = true
		if is_on_floor() and not glitching:
			$AnimatedSprite.play("run")
			
	if velocity.x == 0 and velocity.y == 0 and not Input.is_action_pressed("ui_accept") and not glitching:
		$AnimatedSprite.play("idle")
		
	if is_on_wall() and not glitching:
		$AnimatedSprite.play("wall")
			
	if Input.is_action_pressed("ui_accept") and not glitching:
		$AnimatedSprite.play("attack")
		
	if Input.is_action_just_pressed("ui_up") and not glitching:
		$AnimatedSprite.play("jump")
		$audio_jump.play(0.0)
		
	if Input.is_action_just_pressed("ui_accept") and not glitching:
		$shoot_timer.start()

func receive_damage():
	# Receiveing damage makes you invulnerable while glitching
	if lives > 1 and not glitching:
		glitching = true
		lives -= 1
		$AnimatedSprite.play("glitch")
		$glitch_sound.play()
		emit_signal("hurt")
	elif lives == 1 and not glitching:
		dying = true
		$AnimatedSprite.play("die")
	else:
		pass
	
func receive_heal():
	if lives < 3:
		emit_signal("heal")
		
func pick_coin():
	emit_signal("score_up")

func _on_shoot_timer_timeout():
	if Input.is_action_pressed("ui_accept") and velocity.x == 0 and velocity.y == 0:
		emit_signal("shoot")

func _on_AnimatedSprite_animation_finished():
	if glitching:
		glitching = false
	if dying:
		emit_signal("death")
