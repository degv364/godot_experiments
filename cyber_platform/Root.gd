extends Node

export (PackedScene) var Tutorial
export (PackedScene) var Level01
export (PackedScene) var Level02
export (PackedScene) var Level03

var level

var playing = false

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	
func restore_menu(success):
	$RootUI.visible = true
	$background/Node2D.visible = true
	$menu_music.play()
	playing = false
	if success:
		$RootUI/Finish_label.text = "You Win!"
	else:
		$RootUI/Finish_label.text = "Game over"
	$RootUI/Finish_label.show()
	$RootUI/Finish_label.visible = true
	$RootUI/TutorialButton.visible = false
	$RootUI/Level01Button.visible = false
	$RootUI/Level02Button.visible = false
	$RootUI/Level03Button.visible = false
	$RootUI/message_timer.start()
	
func remove_menu():
	$RootUI.visible = false
	$background/Node2D.visible = false
	playing = true
	$menu_music.stop()

func _on_message_timer_timeout():
	$RootUI/TutorialButton.visible = true
	$RootUI/Level01Button.visible = true
	$RootUI/Level02Button.visible = true
	$RootUI/Level03Button.visible = true
	$RootUI/Finish_label.visible = false

func _on_menu_music_finished():
	if not playing:
		$menu_music.play()


func _on_TutorialButton_pressed():
	remove_menu()
	level = Tutorial.instance()
	level.connect("finish", self, "_on_Level_finish")
	add_child(level)

func _on_Level01Button_pressed():
	remove_menu()
	level = Level01.instance()
	level.connect("finish", self, "_on_Level_finish")
	add_child(level)

func _on_Level02Button_pressed():
	remove_menu()
	level = Level02.instance()
	level.connect("finish", self, "_on_Level_finish")
	add_child(level)

func _on_Level03Button_pressed():
	remove_menu()
	level = Level03.instance()
	level.connect("finish", self, "_on_Level_finish")
	add_child(level)

	
func _on_Level_finish(success):
	restore_menu(success)
	level.queue_free()




